<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Widget extra Plugin
 *
 * A defer load css and js files must be used in conjunction with a addapted asset.php file, also to befound in my bitbucket methiny
 *
 * @author Aat Karelse
 * @package theme asset
 */

class Plugin_Theme_Assets extends Plugin
{
    
    function js_files() {
        $files = $this->attribute('files');
        $files = preg_replace('/\s+/', '', $files);
        $files_a = explode(",", $files);
        foreach ($files_a as $file) {
            Asset::js($file);
        }
        return Asset::render_js();
    }
    
    function js_defer_files() {
        $files = $this->attribute('files');
        $files = preg_replace('/\s+/', '', $files);
        $files_a = explode(",", $files);
        foreach ($files_a as $file) {
            Asset::js($file);
        }
        return Asset::render_js(false, false, true);
    }
    
    function css_files() {
        $files = $this->attribute('files');
        $files = preg_replace('/\s+/', '', $files);
        $files_a = explode(",", $files);
        foreach ($files_a as $file) {
            Asset::css($file);
        }
        return Asset::render_css();
    }
    
    function css_defer_files() {
        $files = $this->attribute('files');
        $files = preg_replace('/\s+/', '', $files);
        $files_a = explode(",", $files);
        foreach ($files_a as $file) {
            Asset::css($file);
        }
        return Asset::render_css(false, false, true);
    }
}
