<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Widget extra Plugin
 *
 * Check if areas exist, are used and if widgets are in area
 *
 * @author Aat Karelse
 * @package widget extras
 */
class Plugin_Widget_extras extends Plugin
{
    function __construct() {
        $this->load->library('widgets/widgets');
        $this->load->model('widgets/widget_m');
    }
    
    public function area_exists() {
        $slug = $this->attribute('slug');
        foreach ($this->widgets->list_areas() as $area) {
            if ($area->slug == $slug) return true;
        }
        return false;
    }
    
    public function area_used() {
        $slug = $this->attribute('slug');
        if (count($this->widgets->list_area_instances($slug)) > 0) return true;
        return false;
    }
    
    public function widget_in_area() {
        $area = $this->attribute('area');
        $name = $this->attribute('name');
        $widgets = $this->widget_m->get_by_area($area);
        foreach ($widgets as $widget) {
            if ($widget->slug == $name) {
                return true;
            }
        }
        return false;
    }
}

/* End of file plugin.php */
